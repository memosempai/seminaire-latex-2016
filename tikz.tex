%%% Présentation de TikZ

\section{Presentation of PGF/TikZ}

\begin{frame}
  \frametitle{PGF/TikZ: vector graphics in \LaTeX}

What are PGF/TikZ ?
\begin{itemize}
  \item PGF is a \bemph{complete} and \bemph{complicated} language for vector graphics,
  \item TikZ is a \bemph{simpler} overlay language to use PGF.
\end{itemize}

\bigskip
They allow to draw complex figures easily. Pros:
\begin{itemize}
  \item The figures are \bemph{integrated} to the \LaTeX{} document (no external file),
  \item Everybody loves vector graphics: always smooth, even for printing,
  \item Very \bemph{rich}, a lot of existing \bemph{examples} available.
\end{itemize}

\bigskip
Cons:
\begin{itemize}
  \item Still \bemph{difficult} to get used to,
  \item May \bemph{slow} the compilation down,
  \item May create \bemph{heavy} PDF files that take time to render.
\end{itemize}
\end{frame}



\section{A few TikZ examples}

\begin{frame}
  \begin{figure}
    \centering
    \tikzexa
    \caption{\footnotesize Architecture Model --- TEXample.net \cite{tikzandpgfexamples}}
  \end{figure}
\end{frame}

\begin{frame}
  \begin{figure}
    \centering
    \tikzexc
    \caption{\footnotesize A Simple Graph --- TEXample.net \cite{tikzandpgfexamples}}
  \end{figure}
\end{frame}

\begin{frame}
  \begin{figure}
    \centering
    \scalebox{0.7}{\tikzexe}
    \caption{\footnotesize Electrical Circuit --- TEXample.net \cite{tikzandpgfexamples}}
  \end{figure}
\end{frame}

\begin{frame}
  \begin{figure}
    \centering
    \scalebox{0.7}{\tikzexd}
    \caption{\footnotesize Oblique Incidence --- TEXample.net \cite{tikzandpgfexamples}}
  \end{figure}
\end{frame}

\begin{frame}
  \begin{figure}
    \centering
    \scalebox{0.7}{\tikzexb}
    \caption{\footnotesize Transmission Electron Microscope System --- TEXample.net \cite{tikzandpgfexamples}}
  \end{figure}
\end{frame}





\section{Usage of TikZ}

\begin{frame}[fragile]
  \frametitle{Preamble}

TikZ must be loaded as a package in the preamble:
\lstinline?\usepackage{tikz}?

\medskip
All TikZ related libraries must also be loaded in the preamble with:
\lstinline?\usetikzlibrary{libraries}?,
allowing to use:
\begin{itemize}
  \item new arrow shapes (\lstinline?arrows?),
  \item shades (\lstinline?shadings?),
  \item line styles (\lstinline?decorations.pathmorphing?),
  \item etc.
\end{itemize}
\end{frame}



\begin{frame}[fragile]
  \frametitle{Crating a figure}

\bigskip
Inside the document, define a TikZ picture with the environment
\lstinline?tikzpicture?,
usually encapsulated in a \lstinline?figure?:

\begin{lstlisting}
\begin{figure}
  \begin{tikzpicture}
    ...
    ...    % Content of the picture
    ...
  \end{tikzpicture}
  \caption{...}
  \label{...}
\end{figure}
\end{lstlisting}
\end{frame}



% Morceaux de la figure d'exemple (pour éviter les copier-coller)
\def \tikzexnodes
{
\node[circle, fill=yellow, draw] (circ) {1};
\node[ellipse, fill=red!50, right of=circ, node distance=3cm]
  (ellipse) {An ellipse};
\node[diamond, fill=blue!50, draw=blue, thick] at (-2, 0) (emptydiamond) {};
}

\def \tikzexedgesa
{
\path[->] (circ) edge (ellipse);
\path[o->, bend right, dashed] (circ) edge (emptydiamond);
}

\def \tikzexedgesb
{
\path[->, bend right] (emptydiamond) edge
  node[below] (arrowback) {0.3}
  (circ);
}

\def \tikzexedges
{
\tikzexedgesa
\tikzexedgesb
}

\begin{frame}[fragile,t]
  \frametitle{Image description with TikZ}

\begin{figure}
  \scalebox{0.5}{\tikzexc}
\end{figure}

A TikZ picture is made of elements defined with commands:

\begin{lstlisting}
  \command[parameters] ... rest of the c££ommand ... ;
\end{lstlisting}

for example, this graph is made of nodes (the blue circles).
Between the nodes are edges (the arrows).
On these edges are labels (like “0.3”) which are also nodes.

All of them are defined with the TikZ commands \lstinline?\node? and \lstinline?\path?.
\end{frame}



\begin{frame}[fragile, t]
  \frametitle{Example: a simple graph}

\begin{figure}
  \begin{tikzpicture}
    \tikzexnodes
  \end{tikzpicture}
\end{figure}

A node is defined with command \lstinline?\node?:
\begin{lstlisting}
\node[£\meta{options}£] (£\meta{name}£) {£\meta{label}£};
\end{lstlisting}

One can specify:
\medskip
\begin{itemize}
  \item the internal name \lstinline?(name)?,
  \item the printed label \lstinline?{label}?,
  \item the shape (\lstinline?circle?, \lstinline?ellipse?, \lstinline?square?...),
    the type of line, the background color,
    the position (absolute or relative to the other nodes)...
\end{itemize}

% Note : « n££ode » permet uniquement d'écrire « node » sans le colorer das le listing
\begin{lstlisting}
\node[circle, fill=yellow, draw] (circ) {1};
\node[ellipse, fill=red!50, right of=circ, node distance=3cm]
  (ellipse) {An ellipse};
\node[diamond, fill=blue!50, draw=blue, thick] at (-2, 0) (emptydiamond) {};
\end{lstlisting}
\end{frame}



\begin{frame}[fragile, t]
  \frametitle{Example: a simple graph}

\begin{figure}
  \begin{tikzpicture}
    \tikzexnodes
    \tikzexedgesa
  \end{tikzpicture}
\end{figure}

An edge can be defined between two nodes with:

\begin{lstlisting}
    \path[£\meta{options}£] (£\meta{origin}£) edge (£\meta{target}£);
\end{lstlisting}

One can define:
\begin{itemize}
  \item the \lstinline?(origin)? and the \lstinline?(cible)? using their internal names,
  \item the type of arrow tip (\lstinline?->?, \lstinline?o->?, \lstinline?-?),
    the curvature (\lstinline?bend right?),
    the type of line (\lstinline?thick?, \lstinline?dashed?)...
\end{itemize}

\begin{lstlisting}
\path[->] (circ) edge (ellipse);
\path[o->, bend right, dashed] (circ) edge (emptydiamond);
\end{lstlisting}

\end{frame}



\begin{frame}[fragile, t]
  \frametitle{Example: a simple graph}

\begin{figure}
  \begin{tikzpicture}
    \tikzexnodes
    \tikzexedges
  \end{tikzpicture}
\end{figure}

A label can be placed on an edge by creating a node alongside this edge.
This is achieved with the keyword \lstinline?node?:

\begin{lstlisting}
\path[->, bend right] (emptydiamond) edge
  node[below] (arrowback) {0.3}
  (circ);
\end{lstlisting}

Note that this new node acts as a normal node
and can be referenced (as an origin or target for an edge, for instance).

\end{frame}



\section{Conclusion about TikZ}

\begin{frame}
  \frametitle{Reuse, reuse, reuse!}

In order to produce great figures with TikZ, it is best to look for examples
to use them as a basis and modify.

\begin{center}
for this: \Huge \rotatebox[origin=c]{15}{\myheart}\bemph{Internet!}\rotatebox[origin=c]{-15}{\myheart}
\end{center}

The best resource is probably the TEXample website,
where a section is dedicated to TikZ \cite{tikzandpgfexamples}
at: \url{http://texample.net/tikz/examples/}.

\bigskip
Furthermore, it is possible to:
\begin{itemize}
  \item define custom themes for similar pictures,
  \item use the available libraries for common diagrams or shapes
    (UML, electrical schematics...),
  \item define \LaTeX commands for the repeated parts.
\end{itemize}
\end{frame}
